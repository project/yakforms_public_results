# What is this module ?

Yakforms Public Results is an enhancement module for [Yakforms](https://www.drupal.org/project/yakforms).

This module allows authors to make their form results public. This option is enabled through the "Edit" page of the form. All users will have access to results through a tab above the form.

# Requirements

This module requires the following to work properly :

* Yakforms, which defines the form1 node type.

This can be downloaded on the [development Git repo](https://framagit.org/framasoft/framaforms), or by using `drush dl yakforms`.

# Credits

Like Yakforms, this module makes heavy use of [Webform](https://www.drupal.org/project/webform/) for Drupal 7.

This module was originally developped by non-profit [Framasoft](https://framasoft.org/en).
